//
//  FlightControllerViewController.h
//  TicketBooking
//
//  Created by Admin on 11.05.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlightControllerViewController : UIViewController
-(void)setCityFrom:(NSString *)cityFrom;
-(void)setCityTo:(NSString*)cityTo;
@end
